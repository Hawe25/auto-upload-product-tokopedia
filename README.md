# auto-upload-product-tokopedia

This python project contains Selenium and BeautifulSoup4 plugins.
This python script can automate upload product to Tokopedia with database in form of CSV. This python project initiated by me, when my clients asking for my help to upload about 3000 product to newly created account.
I know there is a API for uploading a product in Tokopedia, but my client had to open the store before a long weekends, so then instead of uploading one by one, I create this script.

**What should you prepare?**
1. Product Databases
2. Tokopedia account
3. OTP to your phone (only for the first time)

**What do you need?**
1. Python packages (Pandas, Selenium, BeautifulSoup4, pickle)
2. Firefox with selenium packages installed
3. Patience (about 30~60sec per product to be uploaded)


**Debian**

`sudo apt update`

`sudo apt install python3 python3-pip`

`pip3 install pandas selenium BeautifulSoup4`


This project is free to re-use
Credits: [Hans William](sihawe.com)
